#include<cmath>
#include<iostream>

#include "viscosity_h2o_iapws2008.h"

using namespace std;

DynamicViscosityH2O_IAPWS2008::DynamicViscosityH2O_IAPWS2008( const double& TK,
                                  const double& rho )
  :
  TK(TK),
  rho(rho),
  Tstar(647.096),
  rhostar(322.0),
  pstar(22.064e6),
  mustar(1.00e-6),
  x_mu(0.068),
  inv_q_C(1.9),
  q_C(1./inv_q_C),
  inv_q_D(1.1),
  q_D(1./inv_q_D),
  nu(0.63),
  gamma(1.239),
  nu_over_gamma(nu/gamma),
  eta0(0.13),
  Gamma0(0.06),
  TbarR(1.5),
  Hi{{1.67752,2.20462,0.6366564,-0.241605}},
  Hij{{
       { 5.20094e-1, 2.22531e-1, -2.81378e-1, 1.61913e-1, -3.25372e-2, 0., 0.},
       { 8.50895e-2, 9.99115e-1, -9.06851e-1, 2.57399e-1, 0., 0., 0. },
       { -1.08374, 1.88797, -7.72479e-1, 0., 0., 0., 0. },
       { -2.89555e-1, 1.26613, -4.89837e-1, 0., 6.98452e-2, 0., -4.35673e-3 },
       { 0., 0., -2.57040e-1, 0., 0., 8.72102e-3, 0. },
       { 0., 1.20573e-1, 0., 0., 0., 0., -5.93264e-4 }
    }}
{
}

DynamicViscosityH2O_IAPWS2008::~DynamicViscosityH2O_IAPWS2008()
{
}

double DynamicViscosityH2O_IAPWS2008::ViscosityAtTKandRho()
{
  Tbar   = TK/Tstar;
  rhobar = rho/rhostar;
  return Mubar()*mustar;
}

double DynamicViscosityH2O_IAPWS2008::Mubar()
{
  return Muzero()*Mu1();
}

void DynamicViscosityH2O_IAPWS2008::PlotHij()
{
    for(unsigned long i = 0; i < 6; ++i)
    {
        for(unsigned long j = 0; j < 7; ++j){ cout << Hij[i][j] << "\t";}
        cout << endl;
    }
}
  
double DynamicViscosityH2O_IAPWS2008::Muzero()
{
  double denominator = Hi[0];
  double invTbar     = 1.0/Tbar;
  denominator += invTbar*(Hi[1] + invTbar*(Hi[2]+Hi[3]*invTbar));
  return 100.*sqrt(Tbar)/denominator;
}

double DynamicViscosityH2O_IAPWS2008::Mu1()
{
  double rhobar_min_1  = rhobar-1.;
  double invTbar_min_1 = 1.0/Tbar-1.0;

  double sumi(0.);
  for(unsigned long i = 0; i < 6; ++i)
    {
      double sumj(0.);
      for(unsigned long j = 0; j < 7; ++j)
        sumj += Hij[i][j]*pow(rhobar_min_1,j);
      sumi += pow(invTbar_min_1,i)*sumj;
    }
  return exp(rhobar*sumi);
}

/*
  // the actual formulation is computationally very expensive and requires taking
  // derivatives of IAPWS95 that I don't want to hardcode here right now.
  // So we let Mu2 be not in effect; this is also recommended in the IAPWS release if critcal enhancement is not needed,
  // see the actual publication Huber et al. in J. Phys. Chem. Ref. Data (2009) for details
double DynamicViscosityH2O_IAPWS2008::Mu2()
{
  if(!with_critical_enhancement) return 1.0;
  else
    {
      double blubb      = iapws95.drhobardpbar(Tbar,  rhobar );
      double blubbr     = iapws95.drhobardpbar(TbarR, rhobar );
      double deltaXibar = rhobar*(blubb-blubbr*TbarR/Tbar);
      double eta        = eta0*pow(deltaXibar/Gamma0,nu_over_gamma);
      double Y(0.0);
      if(eta > 0.3817016416)
	{
	  double PsiD = acos(1.0+q_D*q_D*eta*eta);
	  double w(sqrt(fabs((q_C*eta-1.0)/(q_C*eta+1.0)))*tan(0.5*PsiD));
	  double L(0.0);
	  if(q_C*eta > 1.0) L = log((1+w)/(1-w));
	  else              L = 2.0*atan(fabs(w));
	  Y += 1.0 / 12.0 * sin(3.0*PsiD);
	  Y -= 1.0 / (4.0*q_C*eta) * sin(2.0*PsiD);
	  Y += 1.0 / (q_C*eta)/(q_C*eta) * ( 1.0-1.25*(q_C*eta)/(q_C*eta) ) * sin(PsiD);
	  Y -= 1.0 / (q_C*eta)/(q_C*eta)/(q_C*eta) * ( (1.0-1.5*(q_C*eta)*(q_C*eta))*PsiD
						       -pow(fabs((q_C*eta)*(q_C*eta)-1.0),1.5)*L );

	}
      else
	{
	  Y = 0.2*q_C*eta*pow(q_D*eta,5)*(1.0
					  - q_C*eta
					  + (q_C*eta)*(q_C*eta)
					  - 765.0/504.0*(q_D*eta)*(q_D*eta));
	}
      return exp(x_mu*Y); 
    }
}
*/
