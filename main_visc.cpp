#include "viscosity_h2o_iapws2008.h"

#include<cmath>
#include<fstream>

using namespace std;

int main(){
  double T{873.};
  double rho{990.};
  DynamicViscosityH2O_IAPWS2008 visc(T,rho);

  ofstream ofile("test.dat");
  for(T = 373.15; T <= 1073.16; T += 100.)
    {
      ofile << T-273.15 << "\t" << visc.ViscosityAtTKandRho() << endl;
    }

}
