#ifndef DynamicViscosityH2O_IAPWS2008_H
#define DynamicViscosityH2O_IAPWS2008_H
#include<array>

class DynamicViscosityH2O_IAPWS2008
{
 public:
  DynamicViscosityH2O_IAPWS2008( const double& TK,
                 const double& rho );
  ~DynamicViscosityH2O_IAPWS2008();
  double ViscosityAtTKandRho(); // naming a bit stupid, also user needs to ensure that rho has been updated before
  void PlotHij();
 private:
  DynamicViscosityH2O_IAPWS2008();

  //  bool  with_critical_enhancement;
  
  const double& TK;
  const double& rho;
  
  const double Tstar;
  const double rhostar;
  const double pstar;
  const double mustar;

  const double x_mu;
  const double inv_q_C;
  const double q_C;
  const double inv_q_D;
  const double q_D;
  const double nu;
  const double gamma;
  const double nu_over_gamma;
  const double eta0;
  const double Gamma0;
  const double TbarR;

  std::array<const double,4>  Hi;
  std::array<std::array<const double,7>, 6> Hij;
  
  double       Tbar;
  double       rhobar;
  double       pbar;
  double       mubar;

  double       Mubar();
  double       Muzero();
  double       Mu1();
//  double       Mu2(); // is there a chance to eliminate compiletime Mu2 if with_critical_enhancement == false?

};

#endif
