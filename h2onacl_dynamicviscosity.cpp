#include "h2onacl_dynamicviscosity.h"
#include<iostream>

using namespace std;

H2ONaCl_DynamicViscosity::H2ONaCl_DynamicViscosity(const double& tCelsius,
                                                   const double& massfractionNaCl,
                                                   const double& pPa)
    :
      tCelsius_(tCelsius),
      massfractionNaCl_(massfractionNaCl),
      pPa_(pPa),
      visc_water(tKStar_,rho_water_)
{
    water = newProp('T', 'd', 1);
}

H2ONaCl_DynamicViscosity::~H2ONaCl_DynamicViscosity()
{
    freeProp(water);
}

double H2ONaCl_DynamicViscosity::DynamicViscosity()
{
    tStar_    = tstar_klyukin();
    tKStar_   = tStar_+273.15;
    water_tp(tKStar_,pPa_, 900., 1.0e-6, water);
    rho_water_ = water->d;
    cout << "rhostar = " << rho_water_<< endl;
    return      visc_water.ViscosityAtTKandRho();
}

double H2ONaCl_DynamicViscosity::tstar_klyukin()
{
    double e1 = -35.9858 * pow( massfractionNaCl_, 0.80017 );
    double e2 = 1. - 1.0e-6*pow( tCelsius_, -0.05239) - 1.32936 * pow( massfractionNaCl_, 0.80017 ) * pow( tCelsius_, 0.-0.05239);
    return e1 + e2 * tCelsius_;
}
