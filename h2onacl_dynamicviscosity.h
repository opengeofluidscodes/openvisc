#ifndef H2ONACL_DYNAMICVISCOSITY_H
#define H2ONACL_DYNAMICVISCOSITY_H

#include<cmath>
#include "viscosity_h2o_iapws2008.h"
#include "steam4.h"

class H2ONaCl_DynamicViscosity
{
public:
    H2ONaCl_DynamicViscosity(const double& tCelsius,
                             const double& massfractionNaCl,
                             const double& pPa);
    ~H2ONaCl_DynamicViscosity();
    double DynamicViscosity();

    double tstar_klyukin();
private:
    const double& tCelsius_;           ///<
    const double& massfractionNaCl_;
    const double& pPa_;
    double rho_water_;
    double tStar_;
    double tKStar_;

    Prop *water;

    DynamicViscosityH2O_IAPWS2008     visc_water;
};
/**
   @class H2ONaCl_DynamicViscosity

   @author Thomas Driesner, ETH Zuerich
   @section contact Contact
   thomas.driesner@erdw.ethz.ch

   @section motivation Motivation
   H2ONaCl_DynamicViscosity implements the formalism of Klyukin et al., Fluid Phase Equilibria 2016, http://dx.doi.org/10.1016/j.fluid.2016.11.002
   The Klyukin formula resides as non-class function in h2onacl_dynamicviscosity_klyukin.h. Here, it is combined with the IAPWS viscosity formulation (which in turn needs the IAPWS95 equation of state for water.

   @section usage Usage
   Construct an instance of "H2ONaCl_DynamicViscosity" with the temperature (in C), Pressure (in Pa) and mass fraction NaCl as constructor variables. This way you do not have to take care of updating these values. See the follwoing code example.

   @code
   double tCelsius{100.};
   double pPascal{10.e6};
   double massfractionNaCl{0.10);
   H2ONaCl_DynamicViscosity   viscosity(tCelsius, pPasacl, massfractionNaCl);
   cout << viscosity.DynamicViscosity() << endl;
   tCelsius = 200.; // H2ONaCl_DynamicViscosit will now automatically know the correct value
   cout << viscosity.DynamicViscosity() << endl;
   @endcode

   @section dependencies Dependencies
   requires
   viscosity_h2o_iapws2008.h/.cpp
   iapws95_density_calculations.h/.pp
   h2onacl_dynamicviscosity_klyukin.h

   @section issues Known issues

   @section testing Testing
*/


#endif // H2ONACL_DYNAMICVISCOSITY_H
