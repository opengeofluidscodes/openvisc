CXX=g++ -std=gnu++11

OBJS = \
main_visc.o \
viscosity_h2o_iapws2008.o

#########################
# Linking
#########################
default : visc

visc : $(OBJS)
	$(CXX) -o $@ $(OBJS)

clean :
	-rm *.o *~ *#

veryclean :
	-rm $(OBJS)

#########################

